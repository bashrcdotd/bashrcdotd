###############################################################################
#
#  bashrcdotd - '.bashrc' file organizer
#
#  Copyright (C) 2023  Julián Yezid Castellanos Pinzón
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Path macros
GITHOOKS_PATH := .githooks

# Default rule
default: init

# Initializes the repository for contributing.
#
# The steps done are:
# 1. Initializes the git configuration to use the provided git hooks.
.PHONY: init
init: --init-githooks

# Initializes the git configuration to use the provided git hooks.
.PHONY: --init-githooks
--init-githooks:
	@echo "Adding $(GITHOOKS_PATH)/ as path for git hooks..."
	@git config --local core.hooksPath $(GITHOOKS_PATH)/
	@echo "Done!"
