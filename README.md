# bashrcdotd

## The `.bashrc` organizer

---

**bashrcdotd** is a shell utility to delegate specific configurations of your `.bashrc` file to separated files located in the `.bashrc.d/` directory, providing a more organized way of structuring your bash profile start scripts and configurations.

---

## Table of Contents

[[_TOC_]]

---

## Installation
TBD.

## Usage
TBD.

## Support
TBD.

## Roadmap
TBD.

## Contributing
Any help will be always welcome! Please follow the [Contributing Guidelines][contributing-guidelines].

## Authors and acknowledgments
This project has been directly and indirectly developed by several people and organizations. Thanks to all of them! Please check the [Acknowledgment Board][acknowledgments].

## License
**bashrcdotd** is licensed under [GNU GENERAL PUBLIC LICENSE Version 3][gpl-v3].


[acknowledgments]: ACKNOWLEDGMENTS.md "Acknowledgments"
[contributing-guidelines]: CONTRIBUTING.md "Contributing Guidelines"
[gpl-v3]: LICENSE "GNU General Public License Version 3"
