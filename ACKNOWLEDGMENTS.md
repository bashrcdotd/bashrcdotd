# Acknowledgements Board
**_for bashrcdotd_** _- The `.bashrc` organizer_

## Contributors

Thanks to these wonderful people 😍 :

<table>
  <tbody>
    <tr>
      <td style="text-align: center; vertical-align: top; width: 20%">
        <a href="https://gitlab.com/jcastellanospinzon">
          <img src="https://secure.gravatar.com/avatar/b08df96cb33f26a8e7cc031771611c9b?s=128\u0026d=identicon" width="128" alt="Julián Castellanos"/>
          <br/>
          <sub><b>Julián Castellanos</b></sub>
        </a>
      </td>
      <td style="text-align: center; vertical-align: top; width: 20%">
        <a href="https://gitlab.com">
          <img src="https://www.clipartmax.com/png/small/67-679556_we-are-looking-for-people-who-want-do-either-one-of-finger.png" width="128" alt="We Are Looking For People Who Want Do Either One Of - Finger Pointing At You @clipartmax.com">
          <br/>
          <sub><b>We Want You!</b></sub>
        </a>
      </td>
    </tr>
  </tbody>
</table>

## Additional Acknowledgments

**bashrcdotd** makes use of the following projects (in alphabetical order):

| Tool                                       | Usage                   |
|--------------------------------------------|-------------------------|
| [ClipartMax][clipartmax]                   | Free icons and cliparts |
| [Commitlint][commitlint]                   | Lint commit messages    |
| [Inkscape][inkscape]                       | Logo creation           |
| [Text To ASCII Art Generator - TAAG][taag] | Shell banner creation   |

Additionally, the following projects have direct influence in the **bashrcdotd** development process  (in alphabetical order):

| Project                                        | Description                                               |
|------------------------------------------------|-----------------------------------------------------------|
| [Conventional commits][conventional-commits]   | Specification for commit messages                         |
| [Keep a Changelog][keep-a-changelog]           | Specification for Changelog document structure            |
| [RFC 2119][rfc-2119]                           | RFC Definitions                                           |
| [Semantic Versioning][semantic-versioning]     | Specification for versioning                              |
| [The Good Docs Project][the-good-docs-project] | Source of multiple documentation templates and guidelines |


[clipartmax]: https://www.clipartmax.com "Clipart Max"
[commitlint]: https://github.com/conventionalcommit/commitlint "commitlint"
[conventional-commits]: https://www.conventionalcommits.org/en/v1.0.0/ "Conventional Commits"
[inkscape]: https://inkscape.org/ "Inkscape"
[keep-a-changelog]: https://keepachangelog.com/en/1.0.0/ "Keep a Changelog"
[rfc-2119]: https://www.ietf.org/rfc/rfc2119.txt "RFC definitions"
[semantic-versioning]: https://semver.org/ "Semantic Versioning"
[taag]: http://patorjk.com/software/taag/ "Text To ASCII Art Generator - TAAG"
[the-good-docs-project]: https://gitlab.com/tgdp "The Good Docs Project"
