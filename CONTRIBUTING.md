# Contributing Guidelines
**_for bashrcdotd_** _- The `.bashrc` organizer_

Welcome! If you're reading this, is because you're interested in making **bashrcdotd** better. Here you can find the general guidelines to be a contributor in the project. We strongly recommend reading this page before starting to help.

Enjoy your reading, and thank you in advance for your help!

> :info: **Note**
> 
> The keywords “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document are to be interpreted as described in [RFC 2119][rfc-2119].

---

## Table of Contents

[[_TOC_]]

---

## Workflow

All the projects are managed using [trunk-based development][trunk-based-development] with `main` as the base branch. Every contribution must be done in a so-called _feature_ branch, which eventually is going to be merged in `main` through a merge request. Direct pushes in the `main` branch are forbidden. 

This project follows [Conventional Commits][conventional-commits], [Semantic Versioning][semantic-versioning] and [Keep a Changelog][keep-a-changelog]. It's recommended to be very familiar with each as we use concepts taken from those projects in this document. In particular the commit type concept is going to be used across branches, merge requests and of course, commits.

### Issues

Any change SHOULD have an [issue][gitlab-issues] describing not only the change motivation but also the proposed solution.
* In the motivation, you need to explain the problem you're trying to solve, the feature you're introducing, or the reason for the change you're doing. This is the context of the solution.
* In the solution, it's quite important not only describing the proposed solution but also why that solution was chosen over any alternatives and any important detail that could be used in the future as reference.

A good issue description is key to accelerate the review process ensuring everyone is aware of the why and the how of the change.

### Merge Requests

As explained before, all changes are merged through [merge requests][gitlab-merge-requests]. Merge requests MUST have a well-defined scope, meaning that every merge request must solve totally or partially one thing at a time, never more than that. As every change SHOULD have a corresponding issue, then one merge request must solve totally or partially one and just one issue. Ideally, the relationship _issue_:_merge request_ is _1_:_1_, if greater than that, probably the issue is too broad and should be split into several ones. In some cases, a single issue can be solved with multiple merge requests.

A well-formed merge request MUST be named using the structure:
```
[[optional issue number]] <change type>: <short description>
```
* **type**: Any of the allowed commit types:
    * `feat`
    * `fix`
    * `docs`
    * `style`
    * `refactor`
    * `perf`
    * `test`
    * `build`
    * `ci`
    * `chore`
    * `revert`
    * `release`
* **issue number** (optional): the issue number has to be explicitly referenced if it exists.
* **short description**: The intention of the merge request.

Some examples of merge requests names are:

* **Correct merge request names** :white_check_mark:
  * `[7] feat: Introducing the frobnicator` _(issue, type and description are correct)_
  * `[15] fix: Fixed frobnication behavior` _(issue, type and description are correct)_
  * `[24] release: Releasing version 1.0` _(issue, type and description are correct)_
  * `chore: Adding license file` _(type and description are correct. Ideally this SHOULD have an issue but is still allowed)_
* **Incorrect merge request names** :x:
  * `[1] Fixing error message` _(No type)_
  * `[1] fix:` _(No description)_
  * `[1] task: Fixing error message` _(Unrecognized type)_
  * `[1] fix Fixing error message` _(No colon after type)_
  * `1 fix: Fixing error message` _(No square brackets wrapping the issue number)_
  * `fix: fix` _(Description is too generic, doesn't explain anything in particular)_

Equally important as the name, the merge request description must include all the information about the changes that are going to be merged. It MUST contain enough information for a reviewer to have all the necessary context to do a thorough revision.

Finally, if the merge request is formally related to an issue, the issue description MUST include: `#<issue number>`. Please follow this guide with further details:
[GitLab - Closing issues automatically][gitlab-issues-automatic-closing].

### Branches

The _feature_ branches MUST be named using the structure:
```
[optional issue number]/<type>/<short description>
```

* **type**: Any of:
  * `feat`
  * `fix`
  * `docs`
  * `style`
  * `refactor`
  * `perf`
  * `test`
  * `build`
  * `ci`
  * `chore`
  * `revert`
  * `release`
* **issue number** (optional): the issue number has to be explicitly referenced if it exists.
* **short description**: The intention of the branch. Use dashes as word separator.

Some examples of branch names are:

* **Correct names** :white_check_mark:
  * `7/feat/Introducing-the-frobnicator` _(issue, type and description are correct)_
  * `15/fix/Fixing-frobnication-behavior-in-component` _(issue, type and description are correct)_
  * `24/release/Version-1-0` _(issue, type and description are correct)_
  * `chore/adding-license` _(type and description are correct. Ideally this SHOULD have an issue but is still allowed)_
* **Incorrect names** :x:
  * `1/Fixing-error-message` _(No type)_
  * `1/feat` _(No description)_
  * `1/task/Fixing error message` _(Unrecognized type)_
  * `fix/fix` _(Description is too generic, doesn't explain anything in particular)_

### Commits

Besides following [Conventional commits][conventional-commits] and its direct relation with [Semantic Versioning][semantic-versioning], all commits MUST be signed. 

All commit messages MUST follow the structure:
```
<type>[optional scope][!]: <short description>

[optional body]

[optional footer(s)]
```

* **type**: Any of: 
  * `feat`
  * `fix`
  * `docs`
  * `style`
  * `refactor`
  * `perf`
  * `test`
  * `build`
  * `ci`
  * `chore`
  * `revert`
  * `release`
* **scope** (optional): The scope MUST consist of a noun describing the section of the codebase the commit affects if it exists, in example:
  * `installer`
  * `tests`
* **!** (optional): Indicates a breaking change in the commit.
* **short description**: The intention of the commit.
* **body** (optional): Detailed description including additional information.
* **footers** (optional): Context references of the commit, in example:
  * `Issue: [issue number]`: The issue number has to be explicitly referenced if it exists.
  * `Signed-off-by: [author]`: The author of the commit has to be explicitly referenced.
  * `Reviewed-by: [reviewer]`: The reviewer of the commit.

Some examples of commit messages are:

* **Correct messages** :white_check_mark: 
  * _(type, description and issue number are correct)_
  ```
  feat: Introducing the frobnicator
    
  Issue: 7
  ```
  * _(type, scope, breaking change alert, description, body, issue and footers are correct)_
  ```
  fix(api)!: Fixed frobnication
  
  The component was fixed correcting behavior under x circumstances.
    
  Issue: 15
  Reviewed-by: Z
  ```
  * (type, description and issue are correct)
  ```
  release/Releasing version 1.0
  
  Issue: 24
  ```
  * _(type and description are correct. Ideally this SHOULD have an issue but is still allowed.)_
  ```
  chore: Adding license file
  ```
* **Incorrect messages** :x:
  * _(No type)_
  ```
  Fixing error message
    
  Issue: 1
  ```
  * _(No description)_
  ```
  fix:

  Issue: 1
  ```
  * _(Unrecognized type)_
  ```
  task: Fixing error message

  Issue: 1
  ```
  * _(No colon after type)_
  ```
  fix Fixing error message

  Issue: 1
  ```
  * _(Description is too generic, doesn't explain anything in particular)_
  ```
  fix: fix

  Issue: 1
  ```

### Git Hooks

To guarantee that some of the above rules in naming branches and commit messages are followed we use [Git Hooks][git-hooks]. After cloning/forking the project you can enable them running:
``` shell
$ ./make init
```

## Build, test and run

### Prerequisites

To be able to successfully build the project you need the following tools in your local setup:

| Tool                                       | Usage                   |
|--------------------------------------------|-------------------------|
| [Commitlint][commitlint]                   | Lint commit messages    |


[commitlint]: https://github.com/conventionalcommit/commitlint "commitlint"
[conventional-commits]: https://www.conventionalcommits.org/en/v1.0.0/ "Conventional Commits"
[git-hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks "Git Hooks"
[gitlab-issues-automatic-closing]: https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically "GitLab Closing Issues Automatically"
[gitlab-issues]: https://docs.gitlab.com/ee/user/project/issues/ "GitLab Issues"
[gitlab-merge-requests]: https://docs.gitlab.com/ee/user/project/merge_requests/ "GitLab Merge Requests"
[keep-a-changelog]: https://keepachangelog.com/en/1.0.0/ "Keep a Changelog"
[rfc-2119]: https://www.ietf.org/rfc/rfc2119.txt "RFC definitions"
[semantic-versioning]: https://semver.org/ "Semantic Versioning"
[trunk-based-development]: https://www.atlassian.com/continuous-delivery/continuous-integration/trunk-based-development "Trunk Based Development"


feat|fix|docs|style|refactor|perf|test|build|ci|chore|revert|release
